﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Text;

namespace OdeToFood.Data.Models
{
    public enum CuisineType
    {
        None,
        Italian,
        Indian,
        French
    }
}
